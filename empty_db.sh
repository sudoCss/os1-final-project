#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			while true; do
				read -r -p "Are you sure you want to empty '$dir'? (y/n): " confirm
				case "$confirm" in
				[Yy]*)
					for file in "$selected_dir"/*.csv; do
						sed -i '2,$d' "$file"
					done

					sed -i 's/is_emptied=0/is_emptied=1/' "$selected_dir/.metadata"
					sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"
					sed -i '/_table_id=/ s/=[0-9]*$/=1/' "$selected_dir/.metadata"

					current_user_role=""
					if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
						current_user_role="owenr"
					elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
						current_user_role="admin"
					else
						current_user_role="other"
					fi
					echo "empty|$dir|$(whoami)|$current_user_role|$(date)" >> "$LOGS_DIR_PATH/$dir.log"

					echo "Database '$dir' has been emptied"
					break
					;;
				[Nn]*)
					echo "Emtpying database canceled"
					break
					;;
				*) echo "Error: Invalid option" ;;
				esac
			done
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
