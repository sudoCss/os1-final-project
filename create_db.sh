#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

db_name=""
while true; do
	read -r -p "Enter a name for your Database: " db_name

	if [ -z "$db_name" ]; then
		echo "Error: You must enter a database name"
	elif [ -d "$DATABASE_PATH/$db_name" ]; then
		echo "Error: Database $db_name already exists"
	else
		break
	fi
done

number=0
while true; do
	echo "Enter:"
	echo "[1] to be a private database"
	echo "[2] to be a public database"
	read -r number

	case $number in
	[12])
		number=$((number - 1))
		break
		;;
	*)
		echo "Error: Invalid option"
		;;
	esac
done

mkdir "$DATABASE_PATH/$db_name"

group_name=$db_name

if grep -q "^$group_name:" /etc/group; then
	echo "Group $group_name already exists, using the existing group.."
else
	sudo groupadd "$group_name"
fi

sudo usermod -aG "$group_name" "$(whoami)"

while read -r username; do
	sudo usermod -aG "$group_name" "$username"
done <"$DATABASE_PATH"/admins

sudo chown :"$group_name" "$DATABASE_PATH/$db_name"

cat <<EOF >$DATABASE_PATH/"$db_name"/.metadata
created_at="$(date)"
last_updated="$(date)"
is_public=$number
is_emptied=1
# table last id

EOF

touch "$LOGS_DIR_PATH/$db_name.log"

chmod 777 "$DATABASE_PATH/$db_name"
chmod 666 "$DATABASE_PATH/$db_name/.metadata"
chmod 666 "$LOGS_DIR_PATH/$db_name.log"

crontab -l >/tmp/mycron
echo "0 0 * * * $LOGS_DIR_PATH/logs_rotation.sh $LOGS_DIR_PATH/$db_name.log # $db_name" >>/tmp/mycron
crontab /tmp/mycron
rm /tmp/mycron

echo "Database $db_name created successfully"
