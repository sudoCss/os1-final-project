#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	is_public=$(grep "is_public" "$selected_dir/.metadata" | cut -d'=' -f2)
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins" || $is_public -eq 1 || groups | grep "$dir"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			csv_files=$(find "$selected_dir" -maxdepth 1 -type f -name "*.csv" -exec basename {} .csv \;)

			if [ -z "$csv_files" ]; then
				echo "Error: No tables found in '$selected_dir' database"
			else
				select file in $csv_files; do
					if [ -n "$file" ]; then
						while true; do
							echo "Enter:"
							echo "[1] to retrieve all data"
							echo "[2] to retrieve filtered data based on criteria"
							read -r option
							case $option in
							1)
								cat "$selected_dir/$file.csv"

								current_user_role=""
								if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
									current_user_role="owenr"
								elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
									current_user_role="admin"
								else
									current_user_role="other"
								fi
								echo "retrieve|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

								break
								;;
							2)
								read -r -p "Enter filter criteria: " filter_criteria
								head -n 1 "$selected_dir/$file.csv"
								tail -n +2 "$selected_dir/$file.csv" | grep -i "$filter_criteria"

								current_user_role=""
								if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
									current_user_role="owenr"
								elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
									current_user_role="admin"
								else
									current_user_role="other"
								fi
								echo "retrieve|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

								break
								;;
							*)
								echo "Error: Invalid option"
								continue
								;;
							esac
							break
						done
						break
					else
						echo "Error: Invalid selection"
					fi

				done
			fi
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
