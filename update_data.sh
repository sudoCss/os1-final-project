#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	is_public=$(grep "is_public" "$selected_dir/.metadata" | cut -d'=' -f2)
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins" || $is_public -eq 1 || groups | grep "$dir"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			csv_files=$(find "$selected_dir" -maxdepth 1 -type f -name "*.csv" -exec basename {} .csv \;)

			if [ -z "$csv_files" ]; then
				echo "Error: No tables found in '$selected_dir' database"
			else
				select file in $csv_files; do
					if [ -n "$file" ]; then
						column_names=()
						IFS=',' read -ra column_names < <(head -n 1 "$selected_dir/$file.csv")

						values=()

						read -r -p "Enter the id of the column you want to update: " selected_id

						for col in "${column_names[@]}"; do
							if [ "$col" = "id" ]; then
								values+=("$selected_id")
							else
								read -r -p "Enter value for '$col'" value
								values+=("$value")
							fi
						done

						new_row=$(printf "%s," "${values[@]}")
						new_row=${new_row%,}

						sed -i "/^$selected_id,/c\\$new_row" "$selected_dir/$file.csv"

						sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"

						current_user_role=""
						if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
							current_user_role="owenr"
						elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
							current_user_role="admin"
						else
							current_user_role="other"
						fi
						echo "update|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

						echo "Row with id '$selected_id' updated in '$file' table"
						break
					else
						echo "Error: Invalid selection"
					fi
				done
			fi
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
