#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			csv_files=$(find "$selected_dir" -maxdepth 1 -type f -name "*.csv" -exec basename {} .csv \;)

			if [ -z "$csv_files" ]; then
				echo "Error: No tables found in '$dir' database"
			else
				select file in $csv_files; do
					if [ -n "$file" ]; then
						while true; do
							echo "Enter:"
							echo "[1] to delete all data"
							echo "[2] to delete filtered data based on criteria"
							read -r option

							case $option in
							1)
								{ head -n 1 "$selected_dir/$file.csv"; } >"$selected_dir/$file.csv.tmp"
								mv "$selected_dir/$file.csv.tmp" "$selected_dir/$file.csv"

								sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"

								current_user_role=""
								if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
									current_user_role="owenr"
								elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
									current_user_role="admin"
								else
									current_user_role="other"
								fi
								echo "delete_data|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

								echo "Deleted all data from '$file' table"
								break
								;;
							2)
								while true; do
									read -r -p "Enter filter criteria: " filter_criteria
									if [ -n "$filter_criteria" ]; then
										{
											head -n 1 "$selected_dir/$file.csv"
											tail -n +2 "$selected_dir/$file.csv" | grep -v -i "$filter_criteria"
										} >"$selected_dir/$file.csv.tmp"

										mv "$selected_dir/$file.csv.tmp" "$selected_dir/$file.csv"

										sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"

										current_user_role=""
										if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
											current_user_role="owenr"
										elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
											current_user_role="admin"
										else
											current_user_role="other"
										fi
										echo "delete_data|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

										echo "Deleted filtered data from '$file' table"
										break
									else
										echo "Error: Invalid input"
									fi
								done
								break
								;;
							*)
								echo "Error: Invalid option"
								continue
								;;
							esac
							break
						done
						break
					else
						echo "Error: Invalid selection"
					fi
				done
			fi
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
