#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			table_name=""
			while true; do
				read -r -p "Enter table name: " table_name
				if [ -z "$table_name" ]; then
					echo "Error: You must enter a table name"
				elif [ -d "$DATABASE_PATH/$table_name" ]; then
					echo "Error: Table $table_name already exists"
				else
					break
				fi
			done

			column_count=0
			while true; do
				read -r -p "Enter columns count: " column_count
				if ! [[ $1 =~ ^[1-9][0-9]+$ ]]; then
					break
				else
					echo "Error: Invalid number"
				fi
			done

			columns=()
			for ((i = 1; i <= column_count; i++)); do
				while true; do
					read -r -p "Enter column $i name: " col_name
					if [ -z "$col_name" ]; then
						echo "Error: You must enter a column name"
					elif [ "$col_name" = "id" ]; then
						echo "Error: You can't name your column 'id' since it's added automatically"
					else
						columns+=("$col_name")
						break
					fi
				done
			done

			{
				echo "id,${columns[*]}" | tr ' ' ','
			} >"$selected_dir/$table_name.csv"

			is_public=$(grep "is_public" "$selected_dir/.metadata" | cut -d'=' -f2)
			if [ "$is_public" -eq 1 ]; then
				chmod 666 "$selected_dir/$table_name.csv"
			else
				chmod 660 "$selected_dir/$table_name.csv"
			fi

			echo "${table_name}_last_id=1" >>"$selected_dir/.metadata"

			sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"

			current_user_role=""
			if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
				current_user_role="owenr"
			elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
				current_user_role="admin"
			else
				current_user_role="other"
			fi
			echo "create_table|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

			echo "Table '${table_name}' with columns '${columns[*]}' created in '$dir' database"
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
