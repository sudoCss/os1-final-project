#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"
			is_emptied=$(grep "is_emptied" "$selected_dir/.metadata" | cut -d'=' -f2)

			if [ "$is_emptied" -eq 1 ]; then
				while true; do
					read -r -p "Are you sure you want to delete '$dir'? (y/n): " confirm
					case "$confirm" in
					[Yy]*)
						rm -rdf "$selected_dir"
						rm -f "$LOGS_DIR_PATH/$dir.log"
						crontab -l >/tmp/mycron
						sed -i "/# $dir/d" /tmp/mycron
						crontab /tmp/mycron
						rm /tmp/mycron

						sudo groupdel "$dir"

						echo "Database '$dir' has been deleted"
						break
						;;
					[Nn]*)
						echo "Deleting database is canceled"
						break
						;;
					*) echo "Error: Invalid option" ;;
					esac
				done
			else
				echo "Error: Database $dir is not empty, you can't delete it"
			fi
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
