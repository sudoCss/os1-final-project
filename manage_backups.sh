#!/bin/bash

DATABASE_PATH="/var/Databases"
BACKUP_DIR_PATH="/opt/backups"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	while true; do
		echo "Enter"
		echo "[1] To backup"
		echo "[2] To restore"
		read -r buorrs

		case $buorrs in
		1)
			compression_type=""
			while true; do
				read -r -p "Enter compression type(zip, gzip, or tar): " compression_type
				case $compression_type in
				zip | gzip | tar)
					break
					;;
				*)
					echo "Error: Invalid option"
					continue
					;;
				esac
				break
			done

			while true; do
				echo "Enter"
				echo "[1] Schedule backups"
				echo "[2] Take instant backup"
				read -r itorsc

				case $itorsc in
				1)
					schedule=""
					while true; do
						read -r -p "Enter prefered schedule(daily, weekly, monthly): " schedule
						case $schedule in
						daily | weekly | monthly)
							break
							;;
						*)
							echo "Error: Invalid option"
							continue
							;;
						esac
					done

					rotation=""
					while true; do
						read -r -p "Enter prefered rotation(date, size): " rotation
						case $rotation in
						date | size)
							break
							;;
						*)
							echo "Error: Invalid option"
							continue
							;;
						esac
					done

					select dir in "${directories[@]}"; do
						if [ -n "$dir" ]; then
							crontab -l >/tmp/mycron
							case $schedule in
							daily)
								echo "0 0 * * * $BACKUP_DIR_PATH/create_backup_and_apply_rotation.sh $dir $DATABASE_PATH/$dir $compression_type $rotation $BACKUP_DIR_PATH/$dir # $dir" >>/tmp/mycron
								;;
							weekly)
								echo "0 0 * * 0 $BACKUP_DIR_PATH/create_backup_and_apply_rotation.sh $dir $DATABASE_PATH/$dir $compression_type $rotation $BACKUP_DIR_PATH/$dir # $dir" >>/tmp/mycron
								;;
							monthly)
								echo "0 0 1 * * $BACKUP_DIR_PATH/create_backup_and_apply_rotation.sh $dir $DATABASE_PATH/$dir $compression_type $rotation $BACKUP_DIR_PATH/$dir # $dir" >>/tmp/mycron
								;;
							esac
							crontab /tmp/mycron
							rm /tmp/mycron

							echo "Backup scheduled"
							break
						else
							echo "Error: Invalid selection"
						fi
					done
					break
					;;
				2)
					read -rp "Enter the date to backup (YYYY-MM-DD): " chosen_update_date
					backup_date=$(date -d "$chosen_update_date" +%s)

					mkdir /tmp/db_bu

					for dir in "${directories[@]}"; do
						if [ -f "$dir/.metadata" ]; then
							metadata_date=$(grep "last_updated" "$DATABASE_PATH/$dir/.metadata" | cut -d "=" -f 2)
							metadata_timestamp=$(date -d "$metadata_date" +%s)

							if ((metadata_timestamp >= backup_date)); then
								cp -r "$DATABASE_PATH/$dir" /tmp/db_bu/
							fi
						fi
					done

					mkdir -p "$BACKUP_DIR_PATH/$dir"
					chmod 777 "$BACKUP_DIR_PATH/$dir"

					case $compression_type in
					zip)
						zip -r "$BACKUP_DIR_PATH/$dir/dbs_updated_after_$chosen_update_date-$(date).zip" /tmp/db_bu/
						;;
					gzip)
						tar -czvf "$BACKUP_DIR_PATH/$dir/dbs_updated_after_$chosen_update_date-$(date).tar.gz" /tmp/db_bu/
						;;
					tar)
						tar -cvf "$BACKUP_DIR_PATH/$dir/dbs_updated_after_$chosen_update_date-$(date).tar" /tmp/db_bu/
						;;
					esac

					echo "Backup created"
					break
					;;
				*)
					echo "Error: Invalid option"
					continue
					;;
				esac
				break
			done
			break
			;;
		2)
			select dir in "${directories[@]}"; do
				if [ -n "$dir" ]; then
					files=()
					while IFS= read -r -d $'\0' file; do
						files+=("$(basename "$file")")
					done < <(find "$BACKUP_DIR_PATH/$dir" -type f \( -name "*.zip" -o -name "*.tar.gz" -o -name "*.tar" \) -print0)

					mkdir -p "/tmp/extract/$dir"

					select file in "${files[@]}"; do
						if [ -n "$file" ]; then
							full_path=$(find "$BACKUP_DIR_PATH/$dir" -type f -name "${file}.*")

							case $full_path in
							*.zip)
								unzip -d /tmp/extract/"$dir" "$full_path"
								;;
							*.tar.gz)
								tar -xzvf "$full_path" "$dir" -C "/tmp/extract/$dir"
								;;
							*.tar)
								tar -xvf "$full_path" "$dir" -C "/tmp/extract/$dir"
								;;
							esac

							echo "Restored to /tmp/extract/$dir"
							break
						else
							echo "Error: Invalid selection"
						fi
					done
					break
				else
					echo "Error: Invalid selection"
				fi
			done
			break
			;;
		*)
			echo "Error: Invalid optoin"
			continue
			;;
		esac
		break
	done
fi
