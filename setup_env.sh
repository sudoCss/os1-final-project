#!/bin/bash

# Intended to be run on a fresh ubuntu docker container

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"
BACKUP_DIR_PATH="/opt/backups"

apt update
apt install sudo
apt install vim
apt install cron
apt install zip
apt install gzip
apt install tar

service cron start

passwd

users=("hussam" "hamza" "karaman" "enawei" "mekdad")

for user in "${users[@]}"; do
	if id "$user" &>/dev/null; then
		echo "User $user already exists"
	else
		useradd -m -U -G sudo -s /bin/bash "$user"
		passwd "$user"
		echo "User $user has been created"
	fi
done

if [ ! -d "$DATABASE_PATH" ]; then
	mkdir "$DATABASE_PATH"
	chmod 777 "$DATABASE_PATH"
fi

if [ ! -d "$LOGS_DIR_PATH" ]; then
	mkdir "$LOGS_DIR_PATH"
	chmod 777 "$LOGS_DIR_PATH"
fi

if [ ! -d "$BACKUP_DIR_PATH" ]; then
	mkdir "$BACKUP_DIR_PATH"
	chmod 777 "$BACKUP_DIR_PATH"
fi

cat <<'EOF' >$LOGS_DIR_PATH/logs_rotation.sh
#!/bin/bash

now=$(date +%s)

while IFS= read -r line
do
    log_date=$(echo $line | awk '{print $3, $4, $5, $6, $7, $8}')

    log_date_sec=$(date -d"$log_date" +%s)

    age=$(( (now - log_date_sec) / 60 / 60 / 24 ))

    if (( age > 10 ))
    then
        sed -i "/$line/d" $1
    fi
done < $1
EOF
chmod 755 "$LOGS_DIR_PATH/logs_rotation.sh"

cat <<'EOF' >$BACKUP_DIR_PATH/create_backup_and_apply_rotation.sh
#!/bin/bash

name=$1
dir_path=$2
comp_type=$3
rotation_pref=$4
output_dir=$5

mkdir -p "${output_dir}"
chmod 777 "${output_dir}"

case $comp_type in
    zip)
        zip -r "${output_dir}/$name-$(date).zip" "${dir_path}"
        ;;
    gzip)
        tar -czvf "${output_dir}/$name-$(date).tar.gz" "${dir_path}"
        ;;
    tar)
        tar -cvf "${output_dir}/$name-$(date).tar" "${dir_path}"
        ;;
esac

case $rotation_pref in
    date)
        ls -t1 "${output_dir}" | tail -n +6 | xargs rm -f
        ;;
    size)
        while [ $(du -sk "${output_dir}" | cut -f1) -gt 100 -a $(ls "${output_dir}" | wc -l) -gt 1 ]; do
            ls -tr1 "${output_dir}" | head -n 1 | xargs rm -f
        done
        ;;
esac
EOF
chmod 755 "$BACKUP_DIR_PATH/create_backup_and_apply_rotation.sh"

cat <<EOF >$DATABASE_PATH/admins
hussam
hamza
karaman
EOF

cat <<EOF >/root/.vimrc
" syntax conf
syntax on
" numbers conf
set relativenumber
set nu
" search conf
set nohls
set smartcase
set ignorecase
set incsearch
" no sounds
set noerrorbells
" tabs conf
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
" line wraping conf
set nowrap
" memory conf
" files open in background even if not saved
set hidden
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile

set scrolloff=8
set signcolumn=yes
set colorcolumn=80
set mouse=a
" clipboard conf
" use the system clipboard buffer (works with gvim)
set clipboard=unnamedplus

set ttym=sgr
" Transparent background if set on terminal
" highlight Normal guibg=none

colorscheme murphy
EOF
