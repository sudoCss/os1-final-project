#! /bin/bash

while true; do
	echo "Enter"
	echo "[1] Create a database"
	echo "[2] Delete a database"
	echo "[3] Empty a database"
	echo "[4] Create a table"
	echo "[5] Delete a table"
	echo "[6] Insert data"
	echo "[7] Update data"
	echo "[8] Retrieve data"
	echo "[9] Delete data"
	echo "[10] Manage logs"
	echo "[11] Manage backups"
	echo "[Q] Quit"

	read -r opt

	case $opt in
	1)
		./create_db.sh
		;;
	2)
		./delete_db.sh
		;;
	3)
		./empty_db.sh
		;;
	4)
		./create_table.sh
		;;
	5)
		./delete_table.sh
		;;
	6)
		./insert_data.sh
		;;
	7)
		./update_data.sh
		;;
	8)
		./retrieve_data.sh
		;;
	9)
		./delete_data.sh
		;;
	10)
		./manage_logs.sh
		;;
	11)
		./manage_backups.sh
		;;
	[Qq])
		break
		;;
	*) echo "Error: invalid option" ;;
	esac
done
