#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_log="$LOGS_DIR_PATH/$dir.log"

			while true; do
				echo "Enter"
				echo "[1] To Print logs"
				echo "[2] To export logs to excel"
				read -r choice
				case "$choice" in
				1)
					cat "$selected_log"
					break
					;;
				2)
					awk -v FS="|" -v OFS="," '{print $1,$2,$3,$4,$5}' "$selected_log" >/tmp/"$dir".xlsx

					echo "Export to excel completed successfully. Results saved to /tmp/$dir.xlsx"

					break
					;;
				*)
					echo "Error: Invalid option"
					;;
				esac
			done
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
