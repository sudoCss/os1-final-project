#!/bin/bash

DATABASE_PATH="/var/Databases"
LOGS_DIR_PATH="/opt/logs"

directories=()

while IFS= read -r -d '' full_dir; do
	dir=$(basename "$full_dir")
	selected_dir="$DATABASE_PATH/$dir"
	owner=$(stat -c '%U' "$selected_dir")
	if [ "$(whoami)" = "$owner" ] || grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
		directories+=("$dir")
	fi
done < <(find "$DATABASE_PATH" -mindepth 1 -maxdepth 1 -type d -print0)

if [ -z "${directories[*]}" ]; then
	echo "Error: There is no databases you can access"
else
	select dir in "${directories[@]}"; do
		if [ -n "$dir" ]; then
			selected_dir="$DATABASE_PATH/$dir"

			csv_files=$(find "$selected_dir" -maxdepth 1 -type f -name "*.csv" -exec basename {} .csv \;)

			if [ -z "$csv_files" ]; then
				echo "Error: No tables found in '$dir' database"
			else
				select file in $csv_files; do
					if [ -n "$file" ]; then
						rm -f "$selected_dir/$file.csv"
						sed -i "/^${file}_last_id=/d" "$selected_dir/.metadata"

						sed -i 's/last_updated=.*/last_updated="'"$(date)"'"/' "$selected_dir/.metadata"

						current_user_role=""
						if [ "$(whoami)" = "$(stat -c '%U' "$selected_dir")" ]; then
							current_user_role="owenr"
						elif grep -q "^$(whoami)$" "$DATABASE_PATH/admins"; then
							current_user_role="admin"
						else
							current_user_role="other"
						fi
						echo "create_table|$dir|$(whoami)|$current_user_role|$(date)" >>"$LOGS_DIR_PATH/$dir.log"

						echo "Table '$(basename "$file")' deleted from '$dir' database"
						break
					else
						echo "Error: Invalid selection"
					fi
				done
			fi
			break
		else
			echo "Error: Invalid selection"
		fi
	done
fi
